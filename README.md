# CerebroX
Machine(Deep) Learning library which supports rapid prototyping features.
## Features
This library is written based on our experiment in field of artificial intelligence research engineering to serves our 
work. There are some features we are going to implement such as:
* Object-oriented network modeling.
* Self-correction training and hyper-parameters tuning.
* Up-to-date machine(deep) learning techniques (we may not public some of them).
## Authors
1. Hieu Tr, Pham (louisnamon@gmail.com)
## License
This library is licensed as [CC-BC-NC 4.0](https://gitlab.com/cerebrox/cerebrox/blob/master/LICENSE.md).
You should cite us if you use this library in your project:
```
Hieu Tr, Pham et al. Cerebrox, a machine learning library. https://gitlab.com/cerebrox/cerebrox. 2020.
```
Or in BibTeX format:
```
@misc{cerebrox2020,
    title={CerebroX, a machine learning library},
    author={Hieu Tr, Pham},
    year={2020},
    howpublished={\url{https://gitlab.com/cerebrox/cerebrox}}
}
```